<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 4/29/2018
 * Time: 10:25 AM
 */

set_time_limit(0);
date_default_timezone_set('UTC');

require __DIR__.'/vendor/autoload.php';
require_once("./includes/configuration.php");

/////// CONFIG ///////
$username = $mpusername;
$password = $mppassword;
$debug = true;
$truncatedDebug = false;
//////////////////////


$amount = 10;
$variance = 9000000;

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

$ig->login($username, $password);
$user = $ig->people->getUserIdForName("");
$user2 = $ig->people->getUserIdForName("");

$target=[
    $user
];

$target2=[
    $user2
];

$recipient = [
    'users' => $target,
];

for($i = 0; $i < $amount; $i++) {
    $random = rand(750000, 1000000);
    $ig->direct->sendText($recipient, "Follow me ".$i." times");
    $finalVariance = $variance + $random;
    usleep($finalVariance);
}