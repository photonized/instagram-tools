<?php
set_time_limit(0);
date_default_timezone_set('UTC');

require __DIR__ . '/vendor/autoload.php';
require_once("./includes/configuration.php");
$rerun = true;

/////// CONFIG ///////
$username = "";
$password = "";
$debug = true;
$truncatedDebug = false;
//////////////////////



$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
try {
    $ig->login($username, $password);
} catch(\Exception $e) {
    echo "Something went wrong! ".$e->getMessage()."\n";
    exit(0);
}

while($rerun) {

    $users = $ig->people->getPendingFriendships()->users;
    $i = 0;
    $igusers[0] = "";

    if (count($users) != 0) {
        $rerun = true;
    } else {
        $rerun = false;
    }

    foreach ($users as $user) {
        $igusers[$i] = $user->pk;
        $i++;
    }

    foreach ($igusers as $iguser) {

        if($iguser == null) {
            break;
        }

        $random = rand(300000,780000);

        $ig->people->rejectFriendship($iguser);

        usleep($random);
    }
}

echo "Follower requests have been DENIED.";