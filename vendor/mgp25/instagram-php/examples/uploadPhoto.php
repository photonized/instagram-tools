<?php

set_time_limit(0);
date_default_timezone_set('UTC');

require __DIR__.'/../../../../vendor/autoload.php';

/////// CONFIG ///////
$username = file_get_contents("../../../../naiosda_username.txt");
$password = file_get_contents("../../../../naiosda_password.txt");
$debug = false;
$truncatedDebug = false;
//////////////////////

$pause_time = file_get_contents("../../../../pause.txt");

$images = scandir("../../../../uploaded/");
$uploaded = 0;

foreach($images as $image) {

	/////// MEDIA ////////
	$photoFilename = $image;
	$captionText = file_get_contents("../../../../caption.txt");
	//////////////////////
	if($photoFilename != "." && $photoFilename != ".." ){
		$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

		try {
		    $ig->login($username, $password);
		} catch (\Exception $e) {
		    echo 'Something went wrong: '.$e->getMessage()."\n";
		    exit(0);
		}

		try {
		    $ig->timeline->uploadPhoto("../../../../uploaded/".$photoFilename, ['caption' => $captionText]);
			$uploaded = $uploaded + 1;
		} catch (\Exception $e) {
		    echo 'Something went wrong: '.$e->getMessage()."\n";
		    echo "<br><br>";
		    var_dump($images);
		}
		//echo "$image uploaded.<br>";
		sleep($pause_time);
	}
	
}

echo $uploaded;