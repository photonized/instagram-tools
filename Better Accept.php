<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 11/5/2018
 * Time: 1:50 PM
 */
set_time_limit(0);
date_default_timezone_set('UTC');

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);

//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());

// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);

require __DIR__ . '/vendor/autoload.php';
require_once("./includes/configuration.php");
$rerun = true;

/////// CONFIG ///////
$username = $mpusername;
$password = $mppassword;
$debug = false;
$truncatedDebug = false;
//////////////////////

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

echo nl2br("Pulled API...\n");

try {
    $ig->login($username, $password);
    echo nl2br("Logged in...\n");
} catch(\Exception $e) {
    try {
        $ig->login($username, $password);
        echo nl2br("Logged in...\n");
    } catch(\Exception $e) {
        echo nl2br("Something went wrong! ".$e->getMessage()."\n");
        exit(0);
    }
}

$bad = [$ig->people->getUserIdForName("lol"), $ig->people->getUserIdForName("communism")];


$total = 0;
$igusers[0] = "";
$users = $ig->people->getPendingFriendships()->users;

while(count($users) != 0) {
    $count = 0;
    foreach($users as $user) {
        $igusers[$count] = $user->pk;
        $count++;
    }
    $total += $count;
    $i = 0;

    foreach($igusers as $iguser){
        $random = rand(300000, 780000);
        //$userinfo = $ig->people->getInfoById($iguser)->getUser()->getUsername();
        if($count>$i) {
            if(!in_array($iguser, $bad)){
                $ig->people->approveFriendship($iguser);
                echo nl2br("Accepted user: ".$iguser.".\n");
            } else {
                $ig->people->rejectFriendship($iguser);
                echo nl2br("Removed user: ".$iguser.".\n");
            }
        }
        $i++;
        usleep($random);
    }
    $users = $ig->people->getPendingFriendships()->users;
}
echo("You have accepted ".$total." followers.");