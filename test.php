
<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 7/19/2018
 * Time: 4:12 PM
 */
// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);

//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());

// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);


for($i = 0; $i < 1000; $i++)
{
    echo 'lol ';
}


/// Now start the program output

echo "Program Output";
