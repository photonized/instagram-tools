<?php
set_time_limit(0);
date_default_timezone_set('UTC');

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);

//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());

// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);

require __DIR__ . '/vendor/autoload.php';
require_once("./includes/configuration.php");
$rerun = true;

/////// CONFIG ///////
$username = $mpusername;
$password = $mppassword;
$debug = false;
$truncatedDebug = false;
//////////////////////


$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
echo nl2br("Pulled InstagramAPI...\n");
try {
    $ig->login($username, $password);
} catch(\Exception $e) {
    echo nl2br("Something went wrong! ".$e->getMessage()."\n");
    echo nl2br("Retrying...\n");
    try {
        $ig->login($username, $password);
    } catch(\Exception $e) {
        echo "Something went wrong! ".$e->getMessage()."\n";
        exit(0);
    }
}
echo nl2br("Logged in...\n");



$mom = $ig->people->getUserIdForName("nataliiajidkova");
$person = $ig->people->getUserIdForName($username);
$bad = array($ig->people->getUserIdForName("nataliiajidkova"), $ig->people->getUserIdForName("communism"));

$followers = $ig->people->getInfoById($person)->getHttpResponse();


echo nl2br("Created the variable for unwanted acceptances...\n").

script();
echo nl2br("Started script...\n");

function script() {

    $total = 0;
    $igusers[0] = "";
    echo nl2br($GLOBALS['person']."\n");


    while($GLOBALS['rerun']) {


        $i = 0;

        $users = $GLOBALS['ig']->people->getPendingFriendships()->users;
        echo nl2br("Got pending friendships...\n");

        if (!count($users) == 0) {
            $GLOBALS['rerun'] = true;
        } else {
            $GLOBALS['rerun'] = false;
            printf("You have accepted ".$total." followers.");
            echo nl2br("You have ".$GLOBALS['ig']->people->getFollowers($GLOBALS['ig']->people->getUserIdForName($GLOBALS['username'])." followers now.\n"));
            exit(0);
        }
        echo nl2br("Counted users in the selected queue...\n");

        foreach ($users as $user) {
            $igusers[$i] = $user->pk;
            $i++;
            echo nl2br($i."\n");
            //usleep(100000);
        }
        echo nl2br("Acquired users...\n");
        $total = $total + $i;

        $i2 = 0;
        foreach ($igusers as $iguser) {

            if($iguser == null) {
                break;
            }

            $random = rand(300000,780000);

            if ($iguser != $GLOBALS['mom']) {
                if($i2 <= $i) {
                $GLOBALS['ig']->people->approveFriendship($iguser);
                echo nl2br("Accepted user: ".$iguser."...\n");
                } else {
                    $GLOBALS['rerun'] = false;
                    printf("You have accepted ".$total." followers.");
                    exit(0);
                }
            } else {
                exit(0);
                $GLOBALS['ig']->people->rejectFriendship($iguser);
                echo nl2br("Declined someone...\n");
            }
            $i2++;
            usleep($random);
        }
    }
}
